# -*- coding: utf-8 -*-


from django.contrib import admin
from files.models import FileModel, UserFile


__author__ = 'Tapo4ek <infdods@yandex.ru>'


@admin.register(FileModel)
class FileModelAdmin(admin.ModelAdmin):
    pass


@admin.register(UserFile)
class UserFileAdmin(admin.ModelAdmin):
    list_display = ('filename', 'user', 'fileobject')