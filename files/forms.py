# -*- coding: utf-8 -*-


import hashlib
from django import forms
from django.utils.translation import ugettext_lazy as _
from files.models import FileModel, UserFile


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class FileModelForm(forms.ModelForm):

    def clean_file(self):
        md5 = hashlib.md5()
        sha1 = hashlib.sha1()
        for chunk in self.cleaned_data['file'].chunks():
            md5.update(chunk)
            sha1.update(chunk)
        # Сделал 2 алгоритма того чтобы избежать коллизий
        self.hash = '{0}${1}'.format(sha1.hexdigest(), md5.hexdigest())
        try:
            file_obj = FileModel.objects.get(hash=self.hash)
            users_with_file = UserFile.objects.filter(fileobject=file_obj).all()
            errors = list()
            for record in users_with_file:
                errors.append(
                    Exception(_('User {0} has this file with name {1}').format(record.user, record.filename))
                )
            raise forms.ValidationError(errors)
        except FileModel.DoesNotExist:
            return self.cleaned_data['file']

    class Meta:
        model = FileModel
        widgets = {
            'file': forms.FileInput(attrs={'class': 'filestyle'}),
        }
        fields = ('file', )