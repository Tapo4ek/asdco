# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib import admin
from files import views as fviews

__author__ = 'Tapo4ek <infdods@yandex.ru>'


urlpatterns = [
    url(r'^$', fviews.FileUploadView.as_view(), name='home'),
    url(r'^load-user-files/$', fviews.FileJSONListView.as_view(), name='load-user-files'),
    url(r'^download/(?P<hash>[0-9a-f]{40}\$[0-9a-f]{32})/$', fviews.FileDownloadView.as_view(), name='file-download'),
    url(r'^remove/(?P<pk>\d+)/$', fviews.FileRemoveView.as_view(), name='file-download'),
]


