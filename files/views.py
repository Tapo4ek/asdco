# -*- coding: utf-8 -*-

__author__ = 'Tapo4ek <infdods@yandex.ru>'


import os
from django.conf import settings
from django.views.generic import View, FormView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from asdco.mixins import LoginRequiredMixin, JSONResponseMixin, FileResponseMixin
from files.forms import FileModelForm
from files.models import UserFile, FileModel


class FileUploadView(LoginRequiredMixin, FormView):

    form_class = FileModelForm
    success_url = reverse_lazy('home')
    template_name = "files/file_list.html"

    def form_valid(self, form):
        """
        Файл уникален
        """
        file_obj = form.save(commit=False)
        file_obj.hash = form.hash
        file_obj.save()
        self.create_user_file(form.cleaned_data['file'].name, file_obj)
        messages.success(self.request, _('File uploaded'))
        return super(FileUploadView, self).form_valid(form)

    def form_invalid(self, form):
        """
        Файл уже есть у кого то из пользователей
        или просто отправили форму без файла и обошли валидацию на сторне js
        """
        for error in form.errors['file']:
            messages.error(self.request, error)

        # Если файл был загружен
        if self.request.FILES:
            name = self.request.FILES['file'].name
            file_object = FileModel.objects.get(hash=form.hash)
            self.create_user_file(name, file_object)
        return super(FileUploadView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        if UserFile.objects.filter(user=request.user).count() > getattr(settings, 'MAX_FILES_PER_USER'):
            messages.error(request, _('Reached maximum number of files'))
            return redirect('home')
        return super(FileUploadView, self).post(request, *args, **kwargs)

    def create_user_file(self, filename, file_object):
        UserFile.objects.create(
            user=self.request.user,
            fileobject=file_object,
            filename=filename
        )


class FileJSONListView(LoginRequiredMixin, JSONResponseMixin, View):

    def get(self, *args, **kwargs):
        """
        Решил не стрелять из пушки по воробьям и не использовать фреймворков типо django-rest-api
        Так как сериализовать мне нужно только в 1м месте
        """
        context = map(lambda x: {
            'record_pk': x.pk,
            'hash': x.fileobject.hash,
            'filename': x.filename
        }, self.request.user.files.all())
        return self.render_to_response(context)


class FileDownloadView(FileResponseMixin, View):

    def get_file_object(self):
        model = FileModel.objects.get(hash=self.kwargs['hash'])
        return model.file


class FileRemoveView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        user_file = UserFile.objects.get(pk=kwargs['pk'], user=request.user)
        if UserFile.objects.filter(fileobject=user_file.fileobject).count() > 1:
            user_file.delete()
        else:
            filepath = user_file.fileobject.file.path
            user_file.fileobject.delete()
            user_file.delete()
            os.remove(filepath)
        return redirect('home')
