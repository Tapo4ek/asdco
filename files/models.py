# -*- coding: utf-8 -*-


from django.db import models
from django.utils.translation import ugettext_lazy as _


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class FileModel(models.Model):

    hash = models.CharField(max_length=73)
    file = models.FileField(upload_to='.')

    def __unicode__(self):
        return self.hash

    class Meta:
        permissions = (("can_see_file", _("Can see file")),)


class UserFile(models.Model):
    filename = models.CharField(max_length=255)
    fileobject = models.ForeignKey('files.FileModel')
    user = models.ForeignKey('auth.User', related_name='files')

    def __unicode__(self):
        return self.filename