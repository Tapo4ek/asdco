# -*- coding: utf-8 -*-


import json
import urllib
from mimetypes import MimeTypes
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


__author__ = 'Tapo4ek <infdods@yandex.ru>'


class LoginRequiredMixin(object):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class JSONResponseMixin(object):

    def render_to_response(self, context, **httpresponse_kwargs):
        return self.get_json_response(self.encode(context), **httpresponse_kwargs)

    def get_json_response(self, content, **httpresponse_kwargs):
        return HttpResponse(content, content_type='application/json', **httpresponse_kwargs)

    def encode(self, context):
        return json.dumps(context)

    def decode(self, context):
        return json.loads(context)


class FileResponseMixin(object):

    filename = None
    file_object = None

    def get(self, request, *args, **kwargs):

        # response = HttpResponse(mimetype=self.get_mimetype())
        response = HttpResponse()
        response['Content-Type'] = 'application/octet-stream'
        response['Content-Disposition'] = 'filename=' + request.GET.get('filename', self.filename)
        response.write(self.get_file_object())
        return response

    def get_mimetype(self):
        mime = MimeTypes()
        url = urllib.pathname2url(self.get_file_object().path)
        return mime.guess_type(url)[0]

    def get_file_object(self):
        if self.file_object is None:
            raise NotImplementedError()
        return self.file_object
