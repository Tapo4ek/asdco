# -*- coding: utf-8 -*-


import os


try:
    from asdco.dev_settings import *
except ImportError as e:
    from asdco.prod_settings import *


BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_DIR = BASE_DIR

ADMINS = (
    ('Peter', 'infdods@yandex.ru'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'sqlite.db'),
    }
}

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ooz4horm)ojdasdasdaKK;lsdg@hdkhgdkhgfkhGG9se!*+o6mwlhth!7r@zbasdsdfdsck=)-z'

TEMPLATE_DEBUG = DEBUG

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

ALLOWED_HOSTS = ['.rft.bz']

# Application definition
INSTALLED_APPS = (
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'asdco',
    'files',
    'registration',
    'bootstrap3',
    'widget_tweaks',
)

# SENTRY_DSN = 'http://e1a8acb4feb447e58b20b6db2485230f:e3c7f719813d4dc8afd826ef67961bbf@sentry.rft.bz/6'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'asdco.urls'

WSGI_APPLICATION = 'asdco.wsgi.application'

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

# Оповещения админов об ошибках приходят с адреса
SERVER_EMAIL = 'errors@asdco.rft.bz'


LANGUAGES = (
    ('ru', 'Russian'),
    ('en', 'English'),
)

# Хранение собщений

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

SITE_ID = 1

# Регистрация

ACCOUNT_ACTIVATION_DAYS = 1
REGISTRATION_AUTO_LOGIN = True

# Максимально разрешенное пользователю количество файлов
MAX_FILES_PER_USER = 100