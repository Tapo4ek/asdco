# -*- coding: utf-8 -*-


import os


__author__ = 'Tapo4ek <infdods@yandex.ru>'


DEBUG = True
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)
EMAIL_PORT = 1025
